import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appFadeIn]'
})
export class FadeInDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.opacity = '0';
    el.nativeElement.style.visibility = 'hidden';
    el.nativeElement.style.top = '10px';
    el.nativeElement.style.transition = '.5s ease-in-out';

    setTimeout(() => {
      el.nativeElement.style.opacity = '1';
      el.nativeElement.style.visibility = 'visible';
      el.nativeElement.style.top = '0';
    });
  }
}
