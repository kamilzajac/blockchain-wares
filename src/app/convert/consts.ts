/**
 * Its probably better to use some lib for date picking, but here is some vanilla solution
 */

const getDays = () => [...Array(32).keys()].filter(e => e !== 0);
const getMonths = () => [...Array(13).keys()].filter(e => e !== 0);
const getYears = () => {
  const now = new Date().getUTCFullYear();
  return Array(now - (now - 100)).fill('').map((v, idx) => now - idx);
};

export const dateSelectValues = {
  days: getDays(),
  months: getMonths(),
  years: getYears(),
};

