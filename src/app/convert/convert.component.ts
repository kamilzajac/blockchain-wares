import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { convertForm } from './convert.form';
import { dateSelectValues } from './consts';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-convert',
  templateUrl: './convert.component.html',
  styleUrls: ['./convert.component.scss']
})
export class ConvertComponent implements OnInit, OnDestroy {
  form: FormGroup = convertForm;
  dateSelectValues = dateSelectValues;
  errors = {};
  subs: Array<Subscription> = [];
  results: Array<number>;
  noResults: boolean;

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.subs.push(this.birthDateForm.valueChanges.subscribe(() => this.errors = {}));
  }

  onSubmit() {
    if (!this.form.valid) {
      this.errors = {...this.errors, ...this.form.errors};
    }

    const regex = /[-]{0,1}[\d]*[.]{0,1}[\d]+/g;
    const data = this.form.get('data').value;
    const matches = data.match(regex);
    this.form.reset();

    if (!matches) {
      return this.noResults = true;
    }
    this.results = [...new Set(matches.map(num => parseFloat(num)).sort((a, b) => a - b))] as Array<number>;
  }

  onClearResults() {
    this.results = null;
    this.noResults = null;
  }

  get birthDateForm() {
    return this.form.get('birthDate') as FormGroup;
  }
}
