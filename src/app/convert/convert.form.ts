import { FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

export const isAdult = (validator: ValidatorFn) => (
  group: FormGroup,
): ValidationErrors | null => {
  const birthDateForm = group.get('birthDate');

  if (!birthDateForm.valid) {
    return {birthDateEmpty: true};
  }
  const year = birthDateForm.get('year').value;
  const month = birthDateForm.get('month').value - 1;
  const day = birthDateForm.get('day').value;

  const dateOfBirth = new Date(year, month, day);
  const ageDifMs = Date.now() - dateOfBirth.getTime();
  const ageDate = new Date(ageDifMs);
  const isOver18 = Math.abs(ageDate.getUTCFullYear() - 1970) >= 18;
  return isOver18 ? null : {notAdult: true};
};

export const convertForm = new FormGroup({
  data: new FormControl('', Validators.required),
  birthDate: new FormGroup({
    day: new FormControl('', Validators.required),
    month: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required)
  })
}, isAdult(Validators.required));
