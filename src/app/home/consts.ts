// I've borrowed some styling ;)

export const particlesStyles = {
  position: 'fixed',
  width: '100%',
  height: '100%',
  'z-index': 1,
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
};

export const particlesParams = {
  particles: {
    number: {
      value: 60,
      density: {
        enable: false,
        value_area: 0
      }
    },
    color: {
      value: '#2185d0'
    },
    shape: {
      type: 'circle',
      stroke: {
        width: 0,
        color: '#000000'
      },
      polygon: {
        nb_sides: 8
      }
    },
    opacity: {
      value: 0.4498141557303954,
      random: true,
      anim: {
        enable: false,
        speed: 1,
        opacity_min: 0.1,
        sync: false
      }
    },
    size: {
      value: 5,
      random: true,
      anim: {
        enable: true,
        speed: 20,
        size_min: 0.1,
        sync: false
      }
    },
    line_linked: {
      enable: true,
      distance: 150,
      color: '#f8f8f8',
      opacity: 0.3447335930860874,
      width: 0
    },
    move: {
      enable: true,
      speed: 0.5,
      direction: 'top',
      random: false,
      straight: false,
      out_mode: 'bounce',
      bounce: false,
      attract: {
        enable: false,
        rotateX: 2565.4592973848366,
        rotateY: 1200
      }
    }
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: {
        enable: false,
        mode: 'repulse'
      },
      onclick: {
        enable: false,
        mode: 'push'
      },
      resize: true
    },
    modes: {
      grab: {
        distance: 400,
        line_linked: {
          opacity: 1
        }
      },
      bubble: {
        distance: 400,
        size: 40,
        duration: 2,
        opacity: 8,
        speed: 3
      },
      repulse: {
        distance: 200,
        duration: 0.4
      },
      push: {
        particles_nb: 4
      },
      remove: {
        particles_nb: 2
      }
    }
  },
  retina_detect: false
};
