import { Component } from '@angular/core';
import { particlesParams, particlesStyles } from './consts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  particlesStyles = particlesStyles;
  particlesParams = particlesParams;
  width = 100;
  height = 100;
}
